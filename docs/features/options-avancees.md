# Options avancées

## Champs conditionnels

Vous pouvez masquer/afficher/forcer un champ suivant les réponses de vos utilisateurs. Par exemple, si une case à cocher "Autre" est sélectionnée, vous pouvez faire apparaître un champ texte "autre" dans laquelle l'utilisateur devra préciser sa réponse.

![champs conditonnel 1](../images/fonctionnalites_forms_conditionnel1.png)
![champs conditionnel 2](../images/fonctionnalites_forms_conditionnel2.png)

[Voir comment faire](../astuces.md#je-souhaite-envoyer-des-mails-en-fonction-des-réponses)

## Règles de validation

Vous pouvez ajouter à votre formulaire, si vous le souhaitez, des règles de validation qui pourront être simples ou complexes. Par exemple : « Vérifier que le champ "âge" soit compris entre 18 et 118 », ou « vérifier que le champ "kilomètres" soit supérieur à 12 ».

![Illustration d'un règle de validation](../images/fonctionnalites_forms_validation.gif)

## Courriels

Vous pouvez envoyer un email à chaque soumission, à vous même, au répondant, aux deux, ou même à qui vous voulez. Cet email peut, si vous le souhaitez, contenir les valeurs saisies dans le formulaire. Pour ce faire vous devez vous rendre dans **Formulaire** > **Courriels**.

![Courriel](../images/fonctionnalites_forms_courriel.png)

Le mail par défaut comportera :

* la date de soumission : ``[submission:date:long]``
* le nom de l'utilisateur⋅trice (si cette personne est connectée à un compte Yakforms) : ``[submission:user]``
* les valeurs soumises : ``[submission:values]``
* le lien vers les résultats : ``[submission:url]``

![modèle de Courriel](../images/fonctionnalites_forms_courriel_modele.png)

Vous avez trois possibilités d'envoi :

* **Standard emails (always send)** : cette fonctionnalité permet d'envoyer un mail après chaque validation de formulaire
* **Confirmation request emails (always send)** : permet d'envoyer un mail à la soumission, avec un lien de validation sur lequel il faut cliquer pour valider la soumission du formulaire
* **Confirmation emails (only send when the confirmation URL is used)** : permet d'envoyer un mail **lorsque le/la répondant⋅e a cliquer sur le lien du mail de confirmation** (configuré ci-dessus)

## Validation de la soumission par email

Vous pouvez demander à ce que la soumission soit confirmée par email. Par exemple si vous voulez éviter qu'une même personne réponde plusieurs fois. Cette dernière recevra alors un email de confirmation. Tant que l'URL dans cet email n'aura pas été cliquée, sa réponse au formulaire sera considérée comme invalide.

Pour ce faire vous devez utiliser **Confirmation request emails (always send)** dans **Formulaire** > **Courriels** et ajouter le jeton `[submission:confirm_url]` dans le modèle de courriel.

Par exemple :

```
Vous avez répondu au formulaire [current-page:title] le [submission:date:long]
Les valeurs soumises sont:
[submission:values]

Merci de bien vouloir confirmer votre participation en cliquant sur : [submission:confirm_url]
```

Vous pouvez ajouter un email à envoyer après confirmation depuis **Confirmation emails (only send when the confirmation URL is used)**.

## Page de confirmation personnalisée

Vous pouvez définir un message de confirmation, ou renvoyer le répondant sur la page de votre choix (votre site web, par exemple)

![confirmation](../images/fonctionnalites_forms_confirmation.png)

Pour cela, allez dans **Paramètres avancés du formulaire**, puis :

1) entrez le message que toute personne ayant répondu au formulaire verra

2) (optionnel) cliquez sur **Explorer les jetons disponibles** pour insérer des messages spéciaux

3) (optionnel) choisir une variable (ici, une date)

4) (optionnel) cette variable affichera la date au format long : ``Vendredi, août 18, 2017 - 14:34``

5) après avoir répondu au questionnaire, la réponse personnalisée s'affiche

![confirmation gif](../images/fonctionnalites_forms_confirmation.gif)

Vous pouvez aussi rediriger les répondant⋅e⋅s vers une adresse particulière en mettant, par exemple, ``https://chatons.org`` pour **Url personnalisée**

## Page d'aperçu personnalisée

Vous pouvez également définir le message qui s'affichera sur la page d'aperçu. Cela permet à l'utilisateur⋅ice de s'assurer une dernière fois que les informations remplies sont correctes avant de les valider définitivement.

Pour activer la page d'aperçu :
1. Rendez-vous sur « Paramètres avancés du formulaire »
2. Dans la section « Page d'aperçu », cliquez sur « Activer la page d'aperçu »
3. Vous pouvez alors modifier les différents éléments de la page d'aperçu : titre, étiquettes des boutons, et message.

Par défaut, l'intégralité des champs remplis par l'utilisateur⋅ice sont visibles sur la page d'aperçu. Vous pouvez également utiliser ces réponses dans message d'aperçu, en appelant utilisant le code **[submission:values:clé_du_champ]**.

Par exemple, le champ « Nom » de ce formulaire a la clé `[new_1608543729018]` :

![aperçu du champ](../images/fonctionnalites_page_apercu_previsualisation.png)

Le message d'aperçu « Je soussigné⋅e [submission:values:new_1608543729018] ... » sera renseigné avec les valeurs remplies par l'utilisateur⋅ice sur la page d'aperçu :

![page d'aperçu](../images/fonctionnalites_page_d_apercu.png)

## Limitation du nombre de soumissions

Vous pouvez limiter le nombre total de soumissions à un chiffre inférieur au maximum fixé par l'administrateur⋅ice du site.

Vous pouvez aussi limiter le nombre de soumissions pour un même utilisateur (identifié par un cookie anonymisé)

![limitations](../images/fonctionnalites_forms_limitations.png)

## Désactivation/fermeture du formulaire.

Par défaut, votre formulaire est actif pendant 6 mois, à l'issue de quoi il sera dépublié : vous seul aurez accès aux résultats, mais personne ne pourra y répondre. Ensuite, à l'issue d'une période de 9 semaines, il sera définitivement supprimé de la base de données.

Vous pouvez le fermer à la soumission quand vous le souhaitez, tout en conservant la possibilité d'y accéder.

Pour fermer un formulaire vous devez :

1. aller dans **Formulaire**
3. cliquer sur **Paramètres avancés du formulaire**
4. dans la section **Paramètres de soumission** aller jusque **Statut de ce formulaire**
5. cocher **Fermé**
6. cliquer sur le bouton **Enregistrer la configuration**

![fermeture formulaire](../images/fonctionnalites_forms_fermeture.png)
