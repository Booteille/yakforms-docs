# Yakforms

## Yakforms, c'est quoi ?
[Yakforms](https://yakforms.org) est un service en ligne libre qui permet de créer, éditer et publier ses formulaires en ligne, puis d’en récolter, analyser et exporter les réponses.

Propulsé par le [Drupal 7](https://drupal.org), associé à de nombreux modules dont [Webform](https://www.drupal.org/project/webform) et [Form builder](drupal.org/project/form_builder), vous pouvez découvrir les fonctionnalités et utilisations possibles de Yakforms en consultant notre [exemple d’utilisation](./exemple-d-utilisation.md).

Voici un exemple de formulaire créé sur Yakforms :

![Formulaire final tel que vu par les utilisateurs](images/yakforms_18_visu2.png)

## Pour aller plus loin

* [Les astuces](astuces.md)
* [Les fonctionnalités](features/creer-un-formulaire.md)
* [Les composants](composants.md)
* [Exporter les données Yakforms en mode publipostage](features/publipostage.md)
* **[Essayer Yakforms](https://yakforms.org/pages/explore.html)**.
* Le [site de Drupal](https://drupal.org), moteur de Yakforms
* [Les détails techniques](https://framagit.org/yakforms/yakforms/-/wikis/) et notamment les instructions pour [installer son propre Yakforms](https://framagit.org/yakforms/yakforms/-/wikis/Installing-Yakforms-through-the-installation-profile), pour un contrôle total de ses données personnelles.
