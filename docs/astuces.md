# Astuces

## Je souhaite recevoir un mail à chaque réponse effectuée sur mon formulaire

Pour recevoir un mail à chaque fois que quelqu'un répond à votre formulaire, vous devez&nbsp;:

  * aller dans l'onglet **Formulaire**
  * cliquer sur **Courriels**
  * cliquer sur le bouton **Ajouter un courriel standard** dans la section **Standard emails (always send)**
  * dans la section **Adresse de courriel du destinataire** vous devez entrer votre adresse mail dans **Personnalisé**
  * vérifier que **Activer envoi** est coché
  * laisser ou modifier le modèle de courriel - le modèle par défaut sera :

```
Soumis le [submission:date:long]
Soumis par l'utilisateur : [submission:user]
Les valeurs soumises sont:
[submission:values]

Les résultats de cette soumission peuvent être vus ici :
[submission:url]
```

c'est-à-dire :

```
Soumis le Lundi, décembre 17, 2018 - 11:15
Soumis par l'utilisateur : Anonyme
Les valeurs soumises sont:

Nombre de personnes: 3
Un message à laisser ?: Je serais probablement en retard !

Les résultats de cette soumission peuvent être vus ici :
https://yakforms.org/node/XXXXX/submission/XXXXXXXX
```

  * cliquer sur **Enregistrer les paramètres de courriel**

## Je souhaite que les personnes qui répondent à mon formulaire reçoivent une copie de leurs réponses par mail

Pour que les personnes reçoivent par mail leurs réponses, vous devez&nbsp;:

  1. ajouter un composant **[Courriel](./composants.md#courriel)** où la personne devra fournir son adresse (pensez à le rendre obligatoire si besoin)
  * cliquer sur **Courriels**
  * cliquer sur le bouton **Ajouter un courriel standard** dans la section **Standard emails (always send)**
  * dans la section **Adresse de courriel du destinataire** vous devez cliquer sur **Composant :** et choisir le titre du composant email que vous avez paramétré
  * vérifier que **Activer envoi** est coché
  * laisser ou modifier le modèle de courriel - le modèle par défaut sera :

```
Soumis le [submission:date:long]
Soumis par l'utilisateur : [submission:user]
Les valeurs soumises sont:
[submission:values]
```

c'est-à-dire :

```
Soumis le Lundi, décembre 17, 2018 - 11:15
Soumis par l'utilisateur : Anonyme
Les valeurs soumises sont:

Nombre de personnes: 3
Un message à laisser ?: Je serais probablement en retard !
```

  * cliquer sur **Enregistrer les paramètres de courriel**

## Je souhaite envoyer des mails en fonction des réponses

Exemple&nbsp;: envoyer un mail à une adresse différente en fonction du ou des comités sélectionné. Pour ce faire, vous devez&nbsp;:

  * créer un composant avec les comités&nbsp;:
    ![composant selections multiples](images/astuces_forms_courriel_comite.png)
  * aller dans l'onglet **Courriels**
  * cliquer sur le bouton **Ajouter un courriel standard**
  * sélectionner le composant dans la section **Adresse de courriel du destinataire**
  * sélectionner les adresses mail en fonction du comité
  * sauvegarder (tout en bas)

![courriel en fonction du composant](images/astuces_forms_courriel_comite2.png)

## Je souhaite un système de CAPTCHA

Si vous souhaitez éviter au maximum les spams sur votre formulaire, vous pouvez créer un système antispam basique. Pour ce faire, vous devez&nbsp;:

  * ajouter un champ texte à mon formulaire (avec, par exemple, la question&nbsp;: **Etes-vous humaine ?**)
  * cocher **Requis** dans l'onglet **Validation** du composant pour obliger à le remplir
  * ajouter une règle **Motif** dans l'onglet **Validation du formulaire**
  * remplir les champs (ici les valeurs sont à ajuster selon vos besoins/envies)&nbsp;:
    * **Nom de la règle**&nbsp;: ``Captcha``
    * **Composant**&nbsp;: ``Etes-vous humaine ?``
    * **Motif**&nbsp;: ``Oui``
    * **Message d'erreur personnalisé**&nbsp;: ``Vous n'êtes pas humaine !`` (si une personne ne répond pas **Oui** elle obtiendra cette erreur)

![Règle Motif Yakforms](images/astuces_forms_captcha.png)

D'autres exemples :

  * Pour montrer que vous n’êtes pas un robot, entrez la réponse à la question suivante : combien font 3 plus sept ? (réponse écrite en chiffres)
  * Pour montrer que vous n’êtes pas un robot, entrez le mot suivant «&nbsp;antispam&nbsp;» ?
  * Pour montrer que vous n’êtes pas un robot, entrez le mot qui manque dans l’expression «&nbsp;prendre ses jambes à son `***`&nbsp;»
  * Soyez créatifs pour en faire d’autres :)

## Je souhaite un champ « autre » libre

Je voudrais qu'en cliquant sur le choix « autre », les répondant⋅e⋅s puissent mettre un choix libre. Pour cela :

* je créé un composant **Boutons radio** (fonctionne aussi avec le composant **Cases à cocher**) ![composant bouton radio](images/astuces_forms_autre1.png)
* je créé un composant « Champ texte » (ici, avec le titre « Autre ? Merci de préciser ») ![composant texte autre](images/astuces_forms_autre2.png)
* j'enregistre
* je vais dans « Champs conditionnels »
* je clique sur le **+** pour ajouter une confition
* je créé la règle : « Si **La mascotte est autre** alors **Autre ? Merci de préciser. est affiché** » (si le champ autre du composant « Boutons radio » est « autre » alors on affiche le composant « Champ texte » - sinon, il ne le sera pas) ![conditions](images/astuces_forms_autre3.png)
* je clique sur le bouton **Enregistrer les conditions**

Résultat animé :

![Animation montrant le résultat](images/astuces_forms_autre.gif)

## Je souhaite afficher des images pour les choix

Vous pouvez le faire en y insérant une photo **déjà disponible sur le web** et en utilisant la balise HTML `img`.

Par exemple, si vous souhaitez insérer l'image du logo Framasoft (disponible en cliquant-droit sur le logo de sur https://framasoft.org/fr/ et en cliquant sur **Copier l'adresse de l'image**)  dans un composant **Boutons radios** (par exemple), vous devez insérer `<img src="https://framasoft.org/img/biglogo-notxt.png" />` dans un champ.

![image dans un composant boutons radios](images/astuces_forms_images_composant_radios.png)

<div class="alert-info alert">
Vous pouvez redimensionner votre image en ajoutant <code>width="9%"</code> et/ou <code>height="100%"</code> ; <code>width</code> pour la largeur (ici en pourcentage) et <code>height</code> pour la hauteur.
</div>

## Je souhaite afficher une image en en-tête de mon Formulaire

### Depuis Framapic (recommandé)

Vous devez commencer par envoyer votre image sur un service d'hébergement temporaire d'images comme [un parmi ces CHATONS (section **Partage d'images**)](https://entraide.chatons.org/fr/) ([voir le tutoriel vidéo](https://docs.framasoft.org/fr/lutim/#tutoriel-vid%C3%A9o)).

Ensuite (dans la partie **Modifier** de votre forumulaire)&nbsp;:

  * cliquez sur l'icône permettant d'ajouter l'adresse de l'image framapic
    ![image montrant l'icône à utiliser pour insérer une image](images/astuces_forms_insert_image_header.png)
  * dans la fenêtre qui s'ouvre&nbsp;:
    1. collez l'adresse de l'image framapic dans le champ **Image URL**
        ![image montrant le champ image url](images/astuces_forms_insert_url.png)
    * cliquez sur **Insert**
  * cliquez sur le bouton **Enregistrer** en bas de la page

### Directement dans Yakforms

Vous avez aussi la possibilité d'insérer directement une image depuis Yakforms. Les fichiers doivent peser moins de 2 Mo\*. Les extensions autorisées sont\* : `png`, `gif`, `jpg` et `jpeg`.

Pour ce faire, vous devez (dans la partie **Modifier** de votre forumulaire)&nbsp;:

  1. cliquer sur **Image**
  * cliquer sur **Parcourir…** pour récupérer l'image sur votre ordinateur
  * cliquer sur **Transférer**
  * cliquer sur le bouton **Enregistrer** en bas de la page

![image montrant les étapes pour ajouter une image](images/astuces_forms_insert_image_header_direct.png)

\* peut varier en fonction de l'instance yakforms

## Je souhaite visualiser mes résultats dans un Framacalc

Dans Yakforms, vous devez :

  * vous rendre dans l'onglet **Résultats**, puis **Téléchargement**
  * sélectionner `Virgule` dans **Format du texte délimité** pour que ce soit au format `.csv` (`c` étant pour `comma` qui signifie virgule en anglais)
  * cliquer sur **Téléchargement** pour télécharger le fichier sur votre ordinateur

Dans Framacalc :

  * allez dans l'onglet **Presse-papier**
  * cochez **Format CSV**
  * collez le contenu de votre fichier `csv` Yakforms dans le cadre
    ![forms dans calc](images/astuces_forms_calc.png)
  * cliquez sur le bouton **Charger le presse-papier de SocialCalc avec ceci**
  * dans l'onglet **Édition**, cliquez sur l'icône **Coller** (ou en faisant `CTRL+v`)

## Je souhaite créer une échelle sémantique différentielle

Pour créer une [échelle sémantique différentielle](https://fr.wikipedia.org/wiki/%C3%89chelle_s%C3%A9mantique_diff%C3%A9rentielle), il est possible d'utiliser le composant **Grille**.

Pour ce faire, vous devez&nbsp;:

  1. mettre votre question en titre
  * dans l'onglet **Options**, mettre des `l` (la lettre `L` en minuscule) ou `|` (`Alt Gr`+`6`) pour créer l'échelle
  * supprimer des questions dans la zone **Questions** pour n'en laisser qu'une
  * mettre dans la question restante vos valeurs (ici, "Froid" et "Chaud") : `Froid | Chaud` ; en mettant `|` (`Alt Gr`+`6`) cela permet de mettre une valeur de chaque côté de la ligne

![forms paramétrage échelle](/images/astuces-forms-echelle.png)

Résultat :

![forms résultat échelle](images/astuces-forms-echelle-resultat.png)

Vous pouvez aussi utiliser la saisie manuelle&nbsp;:
```
0|l
1|l
2|l
3|l
4|l
5|l
```

Pour faire l'exemple ci-dessus.

## Je souhaite rendre un champ obligatoire

Si vous souhaitez que les personnes mettent leur nom (pour un formulaire d'inscription, par exemple), vous pouvez rendre le champ **Nom** obligatoire en&nbsp;:

  1. ajoutant un composant **Champ texte**
  * lui donnant le **Titre** «&nbsp;Nom&nbsp;»
  * en cliquant sur l'onglet **Validation**
  * en cochant **Requis**
  * en cliquant sur **Enregistrer**

![Image illustrant les étapes](images/astuces_champ_requis.png)

Les personnes qui répondent au formulaire ne pourront pas le valider sans avoir entré leur nom dans le champ requis.

<div class="alert alert-info">
  Les champs obligatoires sont marqués d’un <b>*</b> rouge sur le formulaire.
</div>

## Comment mettre en place une validation des réponses par email ?

Vous voulez vérifier que les adresses email entrées par vos sondés sont correctes ? Pour cela, vous pouvez configurer votre formulaire pour qu’après le remplissage du formulaire, la réponse entrée soit validée par le clic sur un lien envoyé à l’adresse email.

  1. Créer un formulaire avec un champ email. (vous pouvez rendre ce champ obligatoire en cliquant sur son onglet **Validation** et en cliquant la case **Requis**)
  * Cliquez sur le bouton **Enregistrer**
  * Allez dans l’onglet **Courriels**
  * Créez un nouvel email de confirmation en cliquant sur **Ajouter une demande de confirmation par email**
  * Cochez **Composant** et sélectionnez le nom du champ de mail.
  * Dans la configuration de cet email de confirmation, il faut ajouter un gabarit personnalisé qui sera envoyé à l’utilisateur pour y insérer le jeton `[submission:confirm_url]` (générant l'url de validation) :

      > Bonjour !
      >
      > Le [submission\:date:long], vous avez répondu au formulaire [current-page:url]
      >
      > *IMPORTANT* : pour confirmer votre participation, veuillez simplement cliquer sur le lien suivant : [submission:confirm_url]
      >
      > Votre participation ne sera pas prise en compte tant que vous n’aurez pas effectué cette action.
      >
      > Les valeurs soumises sont:
      >
      > [submission:values]
      >
      > Cordialement.
      >

   * Enregistrez le tout en cliquant sur le bouton **Enregistrer les paramètres de courriel**.

L’utilisateur recevra alors un email à l’adresse sélectionnée, et devra cliquer sur l’url de confirmation (sinon, sa participation apparaîtra comme "non confirmée" dans les résultats).

Pour aller plus loin, vous pouvez aussi&nbsp;:

  * renvoyer un email de remerciement après la confirmation
  * le renvoyer sur une page dédiée
  * faire expirer l’URL de confirmation après x jours et y heures
  * etc

## Comment limiter à une réponse par adresse email ?

Les paramètres avancés de votre formulaire vous permettent d’activer la limitation à une réponse qui se base sur l’adresse IP et sur un cookie. Cependant dans différents cas d’usage il peut être préférable d’avoir une limitation d’une réponse par adresse email (ou sur un autre champ du formulaire).

Par exemple, si vous souhaitez que deux personnes puissent répondre facilement depuis le même ordinateur, ou si vous voulez que cette limite ne puisse pas être contournée trop facilement (par exemple en utilisant la navigation privée du navigateur, un VPN ou autres).

Lors de la création du champ que vous voulez unique, dans son onglet **Validation**, sélectionnez la case **Unique**, comme ci-dessous&nbsp;:

![image montrant la case "Unique" cochée](images/forms_champ_unique.png)

## Comment ajouter un titre dans le corps de mon formulaire ?

Pour insérer un titre dans le corps du formulaire, vous pouvez, sur la page **Modifier** > **Conception du formulaire**&nbsp;:

  * utiliser l’élément **Balisage** qui se situe dans la partie droite de la page
  * le déplacer où vous souhaitez dans votre sondage
  * éditer le contenu de ce nouvel élément
  * utiliser le HTML pour mettre en page

  Vous pouvez par exemple entrer `<h3>Mon titre</h3>` en replaçant "Mon titre" par le titre que vous souhaitez. Si vous souhaitez un titre plus gros remplacez `<h3>` et `</h3>` par `<h2>` et `</h2>`. Vous pouvez, en créant une nouvelle ligne, ajoutez du texte sous le titre.

  ![titre balisage](images/titre_balisage.png)

## Je souhaite afficher une description sur la première page seulement

La description par défaut d'un formulaire (celle qui se trouve dans l'onglet « Modifier » du formulaire) s'affichera sur toutes les pages de votre formulaire. Si vous souhaitez afficher un texte de description sur la première page uniquement, voici comment procéder :

  1. **Insérer un champ « Balisage »**  : rendez-vous sur l'onglet « Formulaire », et placez le champ au début de votre formulaire.
  * **Renseigner la description** dans ce champ. Vous pouvez la mettre en forme en utilisant la syntaxe HTML. Par exemple :
    ![Champ balisage](images/astuce_description_premiere_page.png)
  * **Sauvegarder** les modifications.

La description s'affiche à présent sur la première page uniquement.


## Comment limiter à une soumission par utilisateur ?

Vous avez la possibilité de limiter la soumission par utilisateur en&nbsp;:

  * allant dans l’onglet **Formulaire**
  * cliquant sur **Paramètres avancés du formulaire**
  * allant dans la section **Limitation des soumissions par utilisateur**
  * cochant **Limiter chaque utilisateur à** `nombre` **soumission(s) pour**
    * **en tout**
    * **toutes les minutes**
    * **toutes les 5 minutes**
    * **par heure**
    * **tous les jours**
    * **toutes les semaines**
  * cliquant sur le bouton **Enregistrer la configuration**

<div class="alert-info alert">Limiter le nombre de soumissions par utilisateur. Un utilisateur est identifié par son nom de connexion s'il est connecté , ou par son adresse IP et un cookie si anonyme.</div>

## Comment pré-remplir certains champs avec des valeurs fournies dans l'url du formulaire ?

  * Éditer la valeur par défaut du champ en question&nbsp;: `[current-page:query:key]`
    ![édition du champ par défaut d'un composant texte](images/astuces_forms_key_url.png)
    Dans l’exemple ci-dessus, `key` est `nom` (`[current-page:query:nom]`) :
  * Ajouter à l'url de votre formulaire la clé du champ et la valeur à fournir ; Par exemple&nbsp;: `?nom=spf` (`https://yakforms.org/mon-form?nom=spf`) pour faire apparaître `spf` dans le composant texte

Plus d'information en anglais sur [la documentation drupal](https://www.drupal.org/node/296453#webform-url-default).

## Afficher / ne pas afficher la barre de progression

Pour (ne pas) afficher la barre de progression vous devez&nbsp;:

1. aller dans l'onglet **Formulaire**
2. cliquer sur **Paramètres avancés du formulaire**
3. cliquer sur la section **Barre de progression**
4. (dé)cocher **Afficher la barre de progression**
5. cliquer sur **Enregistrer la configuration**
